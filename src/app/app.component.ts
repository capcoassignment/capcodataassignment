import { Component } from '@angular/core';
import { TableInfoComponent } from './components/table-info/table-info.component';


@Component({
  selector: 'app-root',
  template: '<app-table-info></app-table-info>',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular  Assessment'}
