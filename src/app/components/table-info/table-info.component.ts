import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Component, OnInit, OnChanges } from '@angular/core';
import { EmployeeService} from '../../services/employee.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';


@Component({
  selector: 'app-table-info',
  templateUrl: './table-info.component.html',
  styleUrls: ['./table-info.component.css'],
  providers :[EmployeeService]
})
export class TableInfoComponent implements OnInit,OnChanges {
users: any[];
columns: string[];
loading = false;
total = 0;
page = 1;
pageSize=10;

  constructor(private empService:EmployeeService) { }

   ngOnInit() {    
      this.columns = this.empService.getColumns();    
      this.loadUserData();
   }
   ngOnChanges(){
      this.loadUserData();
   }
   loadUserData(){
      this.empService.getUsers().subscribe(res=>{
         let lowerLimit : number = (this.page-1)*this.pageSize;
         let upperLimit : number = (this.page) * this.pageSize;
         this.users=res.slice(lowerLimit,upperLimit);
         this.total=res.length;
      });
   }
   goToPage(n: number): void {
    this.page = n;
    this.loadUserData();
  }

  onNext(): void {
    this.page++;
    this.loadUserData();
  }

  onPrev(): void {
    this.page--;
    this.loadUserData();
  }
  changePageSize():void{
    this.loadUserData();
  }
  submitUserData(user:any):void{
     this.empService.postUserData(user);
  }
  
}
