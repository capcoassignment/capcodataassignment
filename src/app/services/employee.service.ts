import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import {users} from '../../assets/User-Info';

@Injectable()

@Injectable()
export class EmployeeService {

  constructor() { }

  getUsers(): Observable<any[]>{
  return Observable.of(users).delay(100);
  }
  getColumns(): string[]{
    return ["name","phone","email","company",
		"date_entry",
		"org_num",
		"address",
		"city",
		"zip",
		"geo",
		"pan",
		"pin",
		"id",
		"status",
		"fee",
		"guid",
		"date_exit",
		"date_first",
		"date_recent",
		"url"]
  }
	postUserData(user):void{
		console.log(user.id);
	}

}
